import { createRouter, createWebHistory } from 'vue-router';
import Register from './../pages/Register.vue';
import Login from './../pages/Login.vue'
import HomeProduct from './../pages/HomeProduct.vue'
import CreateProduct from './../pages/CreateProduct.vue'

const routes = [
  {
    path: '/',
    component: Login
  },
  {
    path: '/register',
    component: Register,
  },
  {
    path: '/homeproduct',
    component: HomeProduct,
  },
  {
    path: '/createproduct',
    component: CreateProduct,
  }
];

const router = createRouter({
  history: createWebHistory(),
  routes
});

export default router;